from turtle import Turtle, Screen, colormode
from move import Move
from random import randint


class RandomWalk(Move):
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        super().__init__(self.__x, self.__y)

    def position(self):
        return self.__x, self.__y

    def move_position(self):
        self.__x = self.x
        self.__y = self.y


colormode(255)
walker = RandomWalk(0, 0)
t = Turtle()
t.penup()
t.pensize(8)
for _ in range(100):
    t.pencolor(randint(0, 255), randint(0, 255), randint(0, 255))
    x,y = walker.position()
    print(x,y)
    t.goto(x,y)
    t.pendown()
    t.speed(1)
    walker.detect_movement_options()
    walker.chose_movement(walker.random_movement_options())
    walker.move_position()

screen = Screen()
screen.exitonclick()

