from random import sample


class MoveOptions:
    def __int__(self, name, x, y):
        self.movements = {
            name: [x,y]
        }


class Move():
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        self.__movements = {}

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, value):
        self.__x = value

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, value):
        self.__y = value

    @property
    def movements(self):
        return self.__movements

    @movements.setter
    def movements(self, value):
        self.__movements = value

    def detect_movement_options(self):
        self.movements['north'] = [self.x + 40, self.y]
        self.movements['south'] = [self.x - 40, self.y]
        self.movements['east'] = [self.x, self.y + 40]
        self.movements['west'] = [self.x, self.y - 40]

    def random_movement_options(self):
        return sample(self.__movements.keys(), 1).pop()

    def chose_movement(self, pos):
        self.x, self.y = self.movements[pos]



